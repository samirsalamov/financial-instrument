package az.interview.task.dao;

import az.interview.task.models.PriceModifier;
import org.junit.*;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import java.io.FileNotFoundException;
import java.math.BigDecimal;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

/**
 * Created by ssalamov on 15-Mar-17.
 */

public class DBConnectionTest {

    private static EntityManagerFactory factory;
    private static EntityManager manager;

    @BeforeClass
    public static void openConnection() throws FileNotFoundException, SQLException {
        Map<String, Object> configOverrides = new HashMap<String, Object>();
        configOverrides.put("hibernate.hbm2ddl.auto", "create");
        factory = Persistence.createEntityManagerFactory("taskPU", configOverrides);
        manager = factory.createEntityManager();
    }

    @AfterClass
    public static void closeConnetion() {
        manager.clear();
        manager.close();
        factory.close();
    }

    @Before
    public void init() {
        manager.getTransaction().begin();
        PriceModifier a = new PriceModifier(1L, "TEST1", BigDecimal.valueOf(1));
        manager.merge(a);
        manager.getTransaction().commit();
    }

    @Test
    public void testGetPriceModifierById() {
        PriceModifier modifier = manager.find(PriceModifier.class, 1L);
        assertNotNull(modifier);
    }
}
