package az.interview.task.models;

import az.interview.task.models.CalculationData;
import az.interview.task.utils.Parameters;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.math.BigDecimal;

/**
 * Created by ssalamov on 15-Mar-17.
 */
public class CalculationDataTest {

    CalculationData data;

    @Before
    public void init() {
        data = new CalculationData();
    }

    @Test
    public void testSumAndIncrement() {
        data.sumAndIncrement(BigDecimal.valueOf(5));
        data.sumAndIncrement(BigDecimal.valueOf(10));
        Assert.assertEquals(data.getSum(), BigDecimal.valueOf(15));
        Assert.assertEquals(data.getCount(), 2);
    }

    @Test
    public void testAverage() {
        data.sumAndIncrement(BigDecimal.valueOf(5));
        data.sumAndIncrement(BigDecimal.valueOf(10));
        data.sumAndIncrement(BigDecimal.valueOf(6));
        Assert.assertEquals(data.getAverage().stripTrailingZeros(), BigDecimal.valueOf(7));
        data.sumAndIncrement(BigDecimal.valueOf(3));
        Assert.assertEquals(data.getAverage().stripTrailingZeros(), BigDecimal.valueOf(6));
    }

    @Test
    public void testMaxValue() {
        data.setMaxValue(BigDecimal.valueOf(5));
        Assert.assertEquals(data.getMaxValue(), BigDecimal.valueOf(5));
        data.setMaxValue(BigDecimal.valueOf(10));
        Assert.assertEquals(data.getMaxValue(), BigDecimal.valueOf(10));
        data.setMaxValue(BigDecimal.valueOf(6));
        Assert.assertEquals(data.getMaxValue(), BigDecimal.valueOf(10));
    }
}
