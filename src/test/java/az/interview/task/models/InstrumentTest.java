package az.interview.task.models;

import az.interview.task.models.Instrument;
import org.junit.Assert;
import org.junit.Test;

import java.math.BigDecimal;

/**
 * Created by ssalamov on 16-Mar-17.
 */
public class InstrumentTest {
    @Test
    public void testBusinessDay() {
        Instrument instrument = Instrument.getInstance("INSTRUMENT1,15-Jan-1998,1");
        Assert.assertTrue(instrument.isBusinessDay());
        instrument = Instrument.getInstance("INSTRUMENT2,24-Jan-1998,2");
        Assert.assertFalse(instrument.isBusinessDay());
    }
}
