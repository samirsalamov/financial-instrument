package az.interview.task;

import az.interview.task.models.CalculationData;
import az.interview.task.models.Instrument;
import az.interview.task.utils.InstrumentNameComparator;

import java.io.*;
import java.text.SimpleDateFormat;
import java.util.*;

/**
 * Created by ssalamov on 17-Mar-17.
 */
public class Test {
    static SimpleDateFormat dateFormatter = new SimpleDateFormat("dd-MMM-yyyy", Locale.ENGLISH);
    private static Date randomDate() {
        GregorianCalendar gc = new GregorianCalendar();
        int year = randBetween(1990, 2010);
        gc.set(gc.YEAR, year);
        int dayOfYear = randBetween(1, gc.getActualMaximum(gc.DAY_OF_YEAR));
        gc.set(gc.DAY_OF_YEAR, dayOfYear);
        int monthOfYear = randBetween(0, 11);
        gc.set(gc.MONTH, monthOfYear);
        return gc.getTime();
    }

    private static int randBetween(int start, int end) {
        return start + (int)Math.round(Math.random() * (end - start));
    }

    private static double randBetween(double start, double end) {
        double leftLimit = 1.5422679D;
        double rightLimit = 150.5422679D;
        double generatedDouble = leftLimit + new Random().nextDouble() * (rightLimit - leftLimit);
        return generatedDouble;
    }

    public static void testFileCreation() throws IOException {
        File file = new File("C:\\Users\\SSamir\\Desktop\\task\\test1.txt");
        FileOutputStream out = new FileOutputStream(file);
        for (int i = 0; i < 100000; i++) {
            out.write(("INSTRUMENT" + randBetween(1, 10) + ","  + dateFormatter.format(randomDate()) + "," + randBetween(140.5422679, 6.4221567)).getBytes());
            out.write("\r\n".getBytes());
            out.flush();
        }
        out.close();
    }

    public static void readFile() throws IOException {
        HashMap<String, CalculationData> datas = new HashMap<>();
        CalculationData data;
        try (BufferedReader reader = new BufferedReader(new FileReader("D:\\test.txt"))) {
            String line = null;
            while ((line = reader.readLine()) != null) {
                Instrument instrument = Instrument.getInstance(line);
                if (!datas.containsKey(instrument.getName())) {
                    datas.put(instrument.getName(), new CalculationData());
                }
                data = datas.get(instrument.getName());
                if (instrument != null && instrument.isBusinessDay()) {
                    data.sumAndIncrement(instrument.getValue());
                    data.setMaxValue(instrument.getValue());
                }
            }
        }
        datas.entrySet().stream()
                .sorted(Map.Entry.comparingByKey(new InstrumentNameComparator())).forEachOrdered(d -> {
            System.out.println("Name: " + d.getKey() + ", Count: " + d.getValue().getCount() + ", Average: " + d.getValue().getAverage() + ", Sum: " + d.getValue().getSum() + ", Max value: " + d.getValue().getMaxValue());
        });

    }

    public static void main(String[] args) throws IOException {
        testFileCreation();
        //readFile();
    }
}
