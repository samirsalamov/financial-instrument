package az.interview.task.services;

import az.interview.task.models.CalculationData;
import az.interview.task.models.Chunk;
import az.interview.task.models.Instrument;
import az.interview.task.services.CalculationService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;

import java.math.BigDecimal;
import java.util.HashMap;

import static org.mockito.Matchers.any;
import static org.mockito.Mockito.atLeast;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;


/**
 * Created by ssalamov on 15-Mar-17.
 */

@RunWith(MockitoJUnitRunner.class)
public class ChunkParsingEngineTest {

    String fileAsString = "INSTRUMENT1,15-Jan-1998,1\r\n" +
            "INSTRUMENT2,23-Jan-1998,2\r\n" +
            "INSTRUMENT3,14-Jan-1998,3\r\n" +
            "INSTRUMENT4,18-Jan-1998,4\r\n" +
            "INSTRUMENT5,15-NOV-1998,5\r\n";

    @Mock CalculationService calculationService;
    @Mock ModifierService modifierService;

    @InjectMocks
    ChunkParsingEngine engine;

    @Test
    public void testParsingOfChunk() {
        Mockito.when(modifierService.getPriceModifier(any(String.class))).thenReturn(BigDecimal.valueOf(1));
        Chunk chunk = new Chunk(fileAsString.getBytes(), "");
        engine.byteChunkProcessing(chunk);
        verify(calculationService).addLocalChunkCalculationDatasToGlobal(any(HashMap.class));
        verify(calculationService).addLocalChunkPriorityQueueToGlobal(any(HashMap.class));
    }
}
