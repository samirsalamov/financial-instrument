package az.interview.task.services;

import az.interview.task.dao.DBConnectorService;
import az.interview.task.models.PriceModifier;
import az.interview.task.services.ModifierService;
import az.interview.task.utils.Parameters;
import junit.framework.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.internal.verification.VerificationModeFactory;
import org.mockito.runners.MockitoJUnitRunner;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Hashtable;
import java.util.List;

import static org.mockito.internal.verification.VerificationModeFactory.times;

/**
 * Created by ssalamov on 16-Mar-17.
 */

@RunWith(MockitoJUnitRunner.class)
public class ModifierServiceTest {

    @Mock
    DBConnectorService dbConnectorService;
    @Mock
    Hashtable<String, PriceModifier> cachedPriceModifiers;

    @InjectMocks
    ModifierService modifierService;

    @Test
    public void testGetPriceModifierMethod() {

        PriceModifier fakeModifier = new PriceModifier(1L, "INSTRUMENT1", BigDecimal.valueOf(1), System.currentTimeMillis() - ((Parameters.SCHEDULER_PRICE_MODIFIER_IN_SECONDS + 1) * 1000));
        cachedPriceModifiers.put("INSTRUMENT1", fakeModifier);
        Mockito.when(cachedPriceModifiers.get("INSTRUMENT1")).thenReturn(fakeModifier);

        fakeModifier = new PriceModifier(1L, "INSTRUMENT1", BigDecimal.valueOf(3));
        Mockito.when(dbConnectorService.findOne(1)).thenReturn(fakeModifier);
        Assert.assertEquals(modifierService.getPriceModifier("INSTRUMENT1"), BigDecimal.valueOf(3));
        Mockito.verify(dbConnectorService, times(1)).findOne(1);

        fakeModifier = new PriceModifier(2L, "INSTRUMENT2", BigDecimal.valueOf(2), System.currentTimeMillis() - ((Parameters.SCHEDULER_PRICE_MODIFIER_IN_SECONDS - 1) * 1000));
        Mockito.when(cachedPriceModifiers.get("INSTRUMENT2")).thenReturn(fakeModifier);

        fakeModifier = new PriceModifier(2L, "INSTRUMENT2", BigDecimal.valueOf(5));
        Mockito.when(dbConnectorService.findOne(2)).thenReturn(fakeModifier);
        Assert.assertEquals(modifierService.getPriceModifier("INSTRUMENT2"), BigDecimal.valueOf(2));
        Mockito.verify(dbConnectorService, Mockito.never()).findOne(2);
    }
}
