package az.interview.task.services;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.*;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.runners.MockitoJUnitRunner;
import org.mockito.stubbing.Answer;

import java.nio.ByteBuffer;
import java.nio.channels.FileChannel;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Semaphore;

import static org.mockito.Matchers.any;
import static org.mockito.Mockito.*;

/**
 * Created by ssalamov on 16-Mar-17.
 */

@RunWith(MockitoJUnitRunner.class)
public class StockFileReaderTest {

    @Mock Semaphore semaphore;
    @Mock ExecutorService executorService;
    @Mock CalculationService calculationService;
    @Mock ModifierService modifierService;
    StockFileReader reader;

    @Before
    public void init()  {
        reader = new StockFileReader(executorService, semaphore, calculationService, modifierService);
        implementAsDirectExecutor(executorService);
    }

    protected void implementAsDirectExecutor(ExecutorService executor) {
        doAnswer(new Answer<Object>() {
            public Object answer(InvocationOnMock invocation) throws Exception {
                ((Runnable) invocation.getArguments()[0]).run();
                return null;
            }
        }).when(executor).submit(any(Runnable.class));
    }

    @Test
    public void read() throws Exception {
        FileChannel channel = Mockito.mock(FileChannel.class);
        ByteBuffer buffer = Mockito.mock(ByteBuffer.class);
        Mockito.when(channel.read(buffer)).thenReturn(1).thenReturn(0);
        reader.readBufferAsChunk(channel, buffer);
        InOrder inOrder = inOrder(channel, buffer, semaphore, executorService);
        inOrder.verify(channel).read(buffer);
        inOrder.verify(semaphore).acquire();
        inOrder.verify(buffer).get(any(byte[].class));
        inOrder.verify(executorService).submit(any(Runnable.class));
        inOrder.verify(semaphore).release();
        inOrder.verify(channel).read(buffer);
    }
}
