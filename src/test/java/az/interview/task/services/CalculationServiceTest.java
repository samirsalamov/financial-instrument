package az.interview.task.services;

import az.interview.task.models.CalculationData;
import az.interview.task.models.Instrument;
import az.interview.task.utils.Parameters;
import javafx.collections.transformation.SortedList;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;

import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.concurrent.ConcurrentHashMap;


/**
 * Created by ssalamov on 17-Mar-17.
 */

@RunWith(MockitoJUnitRunner.class)
public class CalculationServiceTest {

    CalculationService calculationService;
    HashMap<String, CalculationData> localStore;
    HashMap<String, PriorityQueue<Instrument>> localPriorityQueue;

    @Before
    public void initCalculationService() {
        calculationService = new CalculationService();
    }

    @Before
    public void initLocalStore() {
        localStore = new HashMap<>();
        CalculationData data = new CalculationData();
        data.sumAndIncrement(BigDecimal.valueOf(2));
        localStore.put("INSTRUMENT1", data);
        data = new CalculationData();
        data.sumAndIncrement(BigDecimal.valueOf(3));
        localStore.put("INSTRUMENT2", data);
    }

    @Before
    public void initLocalPriorityQueue() {
        localPriorityQueue = new HashMap<>();
        PriorityQueue<Instrument> data = new PriorityQueue<>();
        data.add(Instrument.getInstance("INSTRUMENT4,10-DEC-1998,1"));
        data.add(Instrument.getInstance("INSTRUMENT4,11-DEC-1998,2"));
        localPriorityQueue.put("INSTRUMENT4", data);

        data = new PriorityQueue<>();
        data.add(Instrument.getInstance("INSTRUMENT5,10-DEC-1998,2"));
        data.add(Instrument.getInstance("INSTRUMENT5,11-DEC-1998,3"));
        localPriorityQueue.put("INSTRUMENT5", data);

    }

    @Test
    public void testAddValue() {
        calculationService.addLocalChunkCalculationDatasToGlobal(localStore);
        calculationService.addLocalChunkCalculationDatasToGlobal(localStore);
        calculationService.addLocalChunkPriorityQueueToGlobal(localPriorityQueue);
        calculationService.calculateNewestElementValues();
        ConcurrentHashMap<String, CalculationData> globalStore = calculationService.getGlobalCalculationDatas();
        Assert.assertEquals(globalStore.get("INSTRUMENT1").getAverage().stripTrailingZeros(), BigDecimal.valueOf(2));
        Assert.assertTrue(globalStore.get("INSTRUMENT1").getCount() == 2);
        Assert.assertEquals(globalStore.get("INSTRUMENT2").getAverage().stripTrailingZeros(), BigDecimal.valueOf(3));
        Assert.assertTrue(globalStore.get("INSTRUMENT2").getCount() == 2);
        Assert.assertEquals(globalStore.get("INSTRUMENT4").getSum(), BigDecimal.valueOf(3));
        Assert.assertTrue(globalStore.get("INSTRUMENT4").getCount() == 2);
        Assert.assertEquals(globalStore.get("INSTRUMENT5").getSum(), BigDecimal.valueOf(5));
        Assert.assertTrue(globalStore.get("INSTRUMENT4").getCount() == 2);
    }
}
