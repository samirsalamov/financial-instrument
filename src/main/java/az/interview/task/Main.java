package az.interview.task;

import az.interview.task.dao.DBConnectorService;
import az.interview.task.models.CalculationData;
import az.interview.task.services.CalculationService;
import az.interview.task.services.ModifierService;
import az.interview.task.services.StockFileReader;
import az.interview.task.utils.InstrumentNameComparator;
import az.interview.task.utils.Parameters;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Map;
import java.util.concurrent.*;

/**
 * Created by ssalamov on 10-Mar-17.
 */
public class Main {

    final static Logger logger = LoggerFactory.getLogger(Main.class);

    public static void main(String[] args) {
        try {
            DBConnectorService dbConnector = DBConnectorService.getDbConnector();
            ModifierService modifierService = new ModifierService(dbConnector);
            CalculationService calculationService = new CalculationService();
            Semaphore semaphore = new Semaphore(Parameters.PARALLEL_THREAD_COUNT);
            ExecutorService executorService = Executors.newFixedThreadPool(Parameters.PARALLEL_THREAD_COUNT);
            StockFileReader reader = new StockFileReader(executorService, semaphore, calculationService, modifierService);
            reader.readFileAsBuffer(args.length > 0 && args[0] != null ? args[0] : "example_input.txt");
            executorService.shutdown();
            try {
                executorService.awaitTermination(Long.MAX_VALUE, TimeUnit.NANOSECONDS);
            } catch (InterruptedException ex) {
                logger.error("Executor service termination error: ", ex);
            }
            calculationService.calculateNewestElementValues();
            printFinalResults(calculationService.getGlobalCalculationDatas());
            dbConnector.closeDbConnection();
        }  catch (Exception ex) {
            ex.printStackTrace();
            logger.error("System error occurred: ", ex);
            System.exit(1);
        }
    }

    /**
     * Prints all unique instrument calculation features (Count, Sum, Average, Max value)
     */
    public static void printFinalResults(ConcurrentHashMap<String, CalculationData> globalStore) {
        System.out.println();
        globalStore.entrySet().stream()
                .sorted(Map.Entry.comparingByKey(new InstrumentNameComparator())).forEachOrdered(d -> {
            if (d.getKey().equals("INSTRUMENT1") || d.getKey().equals("INSTRUMENT2")) {
                System.out.println(String.format("Name: %s, Count: %s, Average: %s", d.getKey(), d.getValue().getCount(), d.getValue().getAverage()));
            } else if (d.getKey().equals("INSTRUMENT3")) {
                System.out.println(String.format("Name: %s, Max value: %s", d.getKey(), d.getValue().getMaxValue()));
            } else {
                System.out.println(String.format("Name: %s, Count: %s, Sum: %s", d.getKey(), d.getValue().getCount(), d.getValue().getSum()));
            }
        });
        System.out.println();
    }
}
