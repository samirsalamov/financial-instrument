package az.interview.task.models;

import lombok.Getter;
import lombok.Setter;

import java.math.BigDecimal;
import java.math.RoundingMode;

/**
 * Created by ssalamov on 12-Mar-17.
 * Purpose of this class is to recap final calculations related to each unique instruments
 */
public class CalculationData {

    @Getter private BigDecimal maxValue = new BigDecimal("0");
    @Getter @Setter private BigDecimal sum = new BigDecimal("0");
    @Getter @Setter private BigDecimal value = new BigDecimal("0");
    @Getter @Setter private long count;

    public CalculationData() {
    }

    public CalculationData sumAndIncrement(BigDecimal value) {
        sum = sum.add(value);
        count = count + 1;
        return this;
    }

    public void setMaxValue(BigDecimal value) {
        if (maxValue.compareTo(value) < 0) {
            maxValue = value;
        }
    }

    public BigDecimal getAverage() {
        return count == 0 ? sum : sum.divide(new BigDecimal(count), 9, RoundingMode.HALF_UP);
    }
}
