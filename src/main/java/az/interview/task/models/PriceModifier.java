package az.interview.task.models;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.math.BigDecimal;
import java.text.ParseException;
import java.util.Calendar;

/**
 * Created by ssalamov on 12-Mar-17.
 */
@Entity
@Table(name = "INSTRUMENT_PRICE_MODIFIER")
@NamedQueries({
    @NamedQuery(name="PriceModifier.findAll", query="SELECT p FROM PriceModifier p"),
    @NamedQuery(name="PriceModifier.findByName", query="SELECT p FROM PriceModifier p WHERE p.instrumentName = :name")
})
public class PriceModifier {

    @Id
    @Getter @Setter private Long id;
    @Getter @Setter private String instrumentName;
    @Getter @Setter private BigDecimal multiplier;

    @Transient
    public long lastUpdatedTime;

    public long getLastUpdatedPeriod() {
        long currentTimeMillis = System.currentTimeMillis();
        long seconds = (currentTimeMillis - lastUpdatedTime) / 1000;
        return seconds;
    }

    public PriceModifier(Long id, String name, BigDecimal multiplier) {
        this.id = id;
        this.instrumentName = name;
        this.multiplier = multiplier;
    }

    public PriceModifier(Long id, String name, BigDecimal multiplier, long lastUpdatedTime) {
        this.id = id;
        this.instrumentName = name;
        this.multiplier = multiplier;
        this.lastUpdatedTime = lastUpdatedTime;
    }

    public PriceModifier() {
        this(System.currentTimeMillis());
    }

    public PriceModifier(long lastUpdatedTime) {
        this.lastUpdatedTime = lastUpdatedTime;
    }

    @Override
    public String toString() {
        return "PriceModifier{" +
                "id=" + id +
                ", instrumentName='" + instrumentName + '\'' +
                ", multiplier=" + multiplier +
                ", lastUpdatedTime=" + lastUpdatedTime +
                '}';
    }
}
