package az.interview.task.models;

import az.interview.task.services.StockFileReader;
import lombok.Getter;
import lombok.Setter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.*;
import java.math.BigDecimal;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

/**
 * Created by ssalamov on 12-Mar-17.
 * Purpose of this class is to keep each read line from input file as a object.
 * Also it defines whether date is business day or not.
 */
public class Instrument implements Comparable<Instrument> {

    private static final ThreadLocal<SimpleDateFormat> formatter = new ThreadLocal<SimpleDateFormat>() {
        @Override
        protected SimpleDateFormat initialValue() {
            return new SimpleDateFormat("dd-MMM-yyyy", Locale.ENGLISH);
        }
    };

    @Getter @Setter private String name;
    @Getter @Setter private Calendar date;
    @Getter @Setter private BigDecimal value;
    @Getter @Setter private int month;
    @Getter @Setter private boolean businessDay;

    public Instrument(String name, Date date, BigDecimal value) {
        this.name = name;
        Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        this.month = cal.get(Calendar.MONTH);
        this.businessDay = cal.get(Calendar.DAY_OF_WEEK) != Calendar.SATURDAY && cal.get(Calendar.DAY_OF_WEEK) != Calendar.SUNDAY;
        this.date = cal;
        this.value = value;
    }

    public static Instrument getInstance(String line) {
        Instrument instrument = null;
        try {
            instrument = new Instrument(line.split(",")[0], formatter.get().parse(line.split(",")[1]), new BigDecimal(line.split(",")[2]));
        } catch (Exception ex) {
            System.out.println("Cannot read line with content: " + line);
        }
        return instrument;
    }

    @Override
    public int compareTo(Instrument o) {
        int result = this.getDate().compareTo(o.getDate());
        if (result == 0) {
            result = this.getValue().compareTo(o.getValue());
        }
        return result;
    }

    @Override
    public String toString() {
        return this.getName() + "," + formatter.get().format(this.getDate().getTime()) + "," + this.getValue();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Instrument that = (Instrument) o;

        if (name != null ? !name.equals(that.name) : that.name != null) return false;
        if (date != null ? !date.equals(that.date) : that.date != null) return false;
        return value != null ? value.equals(that.value) : that.value == null;
    }

}
