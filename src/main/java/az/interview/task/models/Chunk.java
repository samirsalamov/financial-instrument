package az.interview.task.models;

import lombok.Getter;
import lombok.Setter;


import java.util.Arrays;

/**
 * Created by ssalamov on 14-Mar-17.
 *
 */
public class Chunk {

    private static int chunkIndex;
    @Getter private byte[] bytes;
    private int lastIndexOfEnter;
    private int length;
    @Getter private String previousBeginningPart;
    @Getter private String remainingPart;

    public Chunk(byte[] bytes) {
        this(bytes, "");
    }

    public Chunk(byte[] bytes, String previousBeginningPart) {
        chunkIndex++;
        this.bytes = bytes;
        this.length = bytes.length;
        this.lastIndexOfEnter = lastIndexOfEOL(bytes);
        this.previousBeginningPart = previousBeginningPart;
        if (lastIndexOfEnter != -1) {
            byte[] remainingPart = Arrays.copyOfRange(bytes, lastIndexOfEnter + 1, bytes.length);
            this.bytes = Arrays.copyOfRange(bytes, 0, lastIndexOfEnter + 1);
            this.remainingPart = new String(remainingPart);
        }
    }

    private int lastIndexOfEOL(byte[] buffer) {
        int lastIndexOfEOL = -1;
        for (int i = buffer.length - 1; i > 0; --i) {
            if (buffer[i] == 10 && buffer[i - 1] == 13) {
                lastIndexOfEOL = i;
                break;
            }
        }
        return lastIndexOfEOL;
    }

    @Override
    public String toString() {
        return "Chunk{" +
                "index=" + chunkIndex +
                ", length=" + length +
                "}";
    }
}
