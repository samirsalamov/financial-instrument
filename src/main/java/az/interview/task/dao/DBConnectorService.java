package az.interview.task.dao;

import az.interview.task.models.PriceModifier;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.persistence.*;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by ssalamov on 14-Mar-17.
 */

public class DBConnectorService {

    final static Logger logger = LoggerFactory.getLogger(DBConnectorService.class);

    private static DBConnectorService dbConnector = new DBConnectorService();

    private EntityManagerFactory factory;
    private EntityManager manager;

    private DBConnectorService() {
        try {
            logger.info("BuildingEntityManager for price modifier");
            Map<String, Object> config = new HashMap<String, Object>();
            config.put("hibernate.hbm2ddl.auto", "create");
            config.put("hibernate.hbm2ddl.import_files", "initial_data.sql");
            factory = Persistence.createEntityManagerFactory("taskPU", config);
            manager = factory.createEntityManager();
        } catch (Exception ex) {
            logger.error("Exception during JPA EntityManager instantiation: ", ex);
        }
    }

    public static DBConnectorService getDbConnector() {
        return dbConnector;
    }

    public PriceModifier findOne(long id) {
        PriceModifier modifier = manager.find(PriceModifier.class, id);
        manager.detach(modifier);
        return modifier;
    }

    public PriceModifier findByName(String name) {
        PriceModifier modifier = null;
        try {
            Query query = manager.createNamedQuery("PriceModifier.findByName");
            query.setParameter("name", name);
            modifier = (PriceModifier) query.getSingleResult();
        } catch (NoResultException ex) {
            //ignore
        } catch (Exception ex) {
            logger.error("Exception during JPA EntityManager findByName: ", ex);
        }
        return modifier;
    }

    public List<PriceModifier> findAll() {
        Query query = manager.createNamedQuery("PriceModifier.findAll");
        List<PriceModifier> items = query.getResultList();
        return items;
    }

    public void closeDbConnection() {
        manager.clear();
        manager.close();
        factory.close();
    }
}
