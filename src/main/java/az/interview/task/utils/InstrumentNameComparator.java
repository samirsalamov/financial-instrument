package az.interview.task.utils;

import java.util.Comparator;

/**
 * Created by ssalamov on 18-Mar-17.
 */
public class InstrumentNameComparator implements Comparator<String> {

    /**
     * String values are assumed to start with name of 'INSTRUMENT' and follow with numbers.
     */
    @Override
    public int compare(String o1, String o2) {
        return Integer.valueOf(o1.substring(10, o1.length())).compareTo(Integer.valueOf(o2.substring(10, o2.length())));
    }
}
