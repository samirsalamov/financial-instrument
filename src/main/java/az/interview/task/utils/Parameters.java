package az.interview.task.utils;

/**
 * Created by ssalamov on 12-Mar-17.
 */
public class Parameters {

    public static int PARALLEL_THREAD_COUNT = 7;
    /**
     * This value is considered to be greater than each line bytes length. Measure unit is byte.
     */
    public static int EACH_BUFFER_LENGTH = 1024 * 1024;
    /**
     * This constant indicated to the other instrument newest elements count apart from INSTRUMENT1, INSTRUMENT2, INSTRUMENT3 which should be calculated as sum
     */
    public static int OTHER_INSTRUMENTS_NEWEST_ELEMENTS_COUNT = 5;
    /**
     * Price modifier of each instrument should be updated from database every given seconds.
     *  and original instrument price should be multiplied to this modifier
     */
    public static int SCHEDULER_PRICE_MODIFIER_IN_SECONDS = 5;
}
