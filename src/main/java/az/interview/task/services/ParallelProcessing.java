package az.interview.task.services;

import az.interview.task.models.Chunk;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.concurrent.*;

/**
 * Created by ssalamov on 12-Mar-17.
 */
public class ParallelProcessing implements Runnable {

    final Semaphore semaphore;
    final Chunk chunk;
    private CalculationService calculationService;
    private ModifierService modifierService;

    public ParallelProcessing(Semaphore semaphore, Chunk chunk, CalculationService calculationService, ModifierService modifierService) {
        this.semaphore = semaphore;
        this.chunk = chunk;
        this.calculationService = calculationService;
        this.modifierService = modifierService;
    }

    @Override
    public void run() {
        ChunkParsingEngine engine = new ChunkParsingEngine(calculationService, modifierService);
        engine.byteChunkProcessing(chunk);
        semaphore.release();
    }
}
