package az.interview.task.services;

import az.interview.task.dao.DBConnectorService;
import az.interview.task.models.PriceModifier;
import az.interview.task.utils.Parameters;

import javax.persistence.Query;
import java.math.BigDecimal;
import java.util.Hashtable;
import java.util.List;

/**
 * Created by ssalamov on 15-Mar-17.
 */
public class ModifierService {

    private DBConnectorService connectorService;

    private Hashtable<String, PriceModifier> cachedPriceModifiers = new Hashtable<>();

    public ModifierService(DBConnectorService connectorService) {
        this.connectorService = connectorService;
    }

    public ModifierService(DBConnectorService connectorService, Hashtable<String, PriceModifier> cachedPriceModifiers) {
        this.connectorService = connectorService;
        this.cachedPriceModifiers = cachedPriceModifiers;
    }

    /**
     * Will be reloaded Price Modifier if last updated period of value from database greater than in given seconds,
     * otherwise Price Modifier will be returned from cache.
     */
    public synchronized BigDecimal getPriceModifier(String name) {
        PriceModifier cached = cachedPriceModifiers.get(name);
        if (cached == null) {
            cached = connectorService.findByName(name);
        } if (cached != null && cached.getLastUpdatedPeriod() >= Parameters.SCHEDULER_PRICE_MODIFIER_IN_SECONDS) {
            cached = connectorService.findOne(cached.getId());
        }
        if (cached != null) {
            cachedPriceModifiers.put(name, cached);
            return cached.getMultiplier();
        }
        return new BigDecimal("1");
    }
}
