package az.interview.task.services;

import az.interview.task.models.CalculationData;
import az.interview.task.models.Instrument;
import az.interview.task.utils.Parameters;
import java.util.*;
import java.util.concurrent.ConcurrentHashMap;

/**
 * Created by ssalamov on 12-Mar-17.
 */
public class CalculationService {

    private ConcurrentHashMap<String, CalculationData> globalStore = new ConcurrentHashMap<>();
    private ConcurrentHashMap<String, PriorityQueue<Instrument>> globalQueue = new ConcurrentHashMap<>();

    /**
     * Each thread at the end of the Chunk processing make local priority queue related to other instruments excluding INSTRUMENT1, INSTRUMENT2, INSTRUMENT3.
     * Then this queue is added to global queue.
     */
    public synchronized void addLocalChunkPriorityQueueToGlobal(HashMap<String, PriorityQueue<Instrument>> chunkLocalPriorityQueue) {
        chunkLocalPriorityQueue.forEach((k,v)->{
            PriorityQueue<Instrument> queue = globalQueue.containsKey(k) ? globalQueue.get(k) : new PriorityQueue<>();
            queue.addAll(v);
            int extraElementsCount = queue.size() > Parameters.OTHER_INSTRUMENTS_NEWEST_ELEMENTS_COUNT ? queue.size() - Parameters.OTHER_INSTRUMENTS_NEWEST_ELEMENTS_COUNT : 0;
            for (int i = 0; i < extraElementsCount; i++) {
                queue.poll();
            }
            globalQueue.put(k, queue);
        });
    }

    /**
     * Each thread at the end of the Chunk processing make local calculation data store related to INSTRUMENT1, INSTRUMENT2, INSTRUMENT3.
     * Then this local store is added to global store.
     */
    public synchronized void addLocalChunkCalculationDatasToGlobal(HashMap<String, CalculationData> chunkLocalStore) {
        chunkLocalStore.forEach((k,v)->{
            CalculationData data = globalStore.containsKey(k) ? globalStore.get(k) : new CalculationData();
            data.setSum(data.getSum().add(v.getSum()));
            data.setCount(data.getCount() + v.getCount());
            data.setMaxValue(v.getMaxValue());
            globalStore.put(k, data);
        });
    }

    /**
     * Calculates sum of the newest instrument elements values excluding INSTRUMENT1, INSTRUMENT2, INSTRUMENT3.
     */
    public void calculateNewestElementValues() {
        globalQueue.forEach((k,v) -> {
            CalculationData data = new CalculationData();
            v.stream().forEach(instrument -> globalStore.put(instrument.getName(), data.sumAndIncrement(instrument.getValue())));
        });
    }

    public ConcurrentHashMap<String, CalculationData> getGlobalCalculationDatas() {
        return globalStore;
    }
}