package az.interview.task.services;

import az.interview.task.models.CalculationData;
import az.interview.task.models.Chunk;
import az.interview.task.utils.Parameters;
import javassist.Modifier;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import java.io.*;
import java.nio.ByteBuffer;
import java.nio.channels.FileChannel;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Semaphore;

/**
 * Created by ssalamov on 12-Mar-17.
 */
public class StockFileReader {

    private Semaphore semaphore;
    private ExecutorService executorService;
    private CalculationService calculationService;
    private ModifierService modifierService;

    public StockFileReader(ExecutorService executorService, Semaphore semaphore, CalculationService calculationService, ModifierService modifierService) {
        this.semaphore = semaphore;
        this.executorService = executorService;
        this.calculationService = calculationService;
        this.modifierService = modifierService;
    }

    /**
     * Read file from channel into byte buffer and send it to chunk processing until its end within main thread.
     */
    public void readFileAsBuffer(String filePath) throws Exception {
        RandomAccessFile aFile = new RandomAccessFile(filePath, "r");
        FileChannel inChannel = aFile.getChannel();
        ByteBuffer buffer = ByteBuffer.allocate(Parameters.EACH_BUFFER_LENGTH);
        readBufferAsChunk(inChannel, buffer);
        inChannel.close();
        aFile.close();
    }

    /**
     * Creates Chunk objects from read byte buffers and separate it into two part from beginning to EOL and
     * from EOL to end of byte buffer. Each remaining part from previous Chunk concatenates with begnning of next Chunk.
     */
    public void readBufferAsChunk(FileChannel inChannel, ByteBuffer buffer) throws Exception {
        int read = 0;
        String previousRemainingPart = "";
        while((read = inChannel.read(buffer)) > 0) {
            semaphore.acquire();
            buffer.flip();
            byte[] bytes = new byte[read];
            buffer.get(bytes);
            buffer.clear();
            Chunk chunk = new Chunk(bytes, previousRemainingPart);
            previousRemainingPart = chunk.getRemainingPart();
            runAsAsync(chunk);
        }
        // read last lines if not ends with ENTER
        if (previousRemainingPart != null && !previousRemainingPart.isEmpty()) {
            Chunk chunk = new Chunk(previousRemainingPart.getBytes());
            runAsAsync(chunk);
        }
    }


    /**
     * Send chunks into parallel thread processing
     */
    private void runAsAsync(Chunk chunk) {
        ParallelProcessing worker = new ParallelProcessing(semaphore, chunk, calculationService, modifierService);
        executorService.submit(worker);
    }
}
