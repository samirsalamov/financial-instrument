package az.interview.task.services;

import az.interview.task.dao.DBConnectorService;
import az.interview.task.models.CalculationData;
import az.interview.task.models.Chunk;
import az.interview.task.models.Instrument;
import az.interview.task.utils.Parameters;

import java.io.*;
import java.math.BigDecimal;
import java.util.*;
import java.util.concurrent.ConcurrentHashMap;

/**
 * Created by ssalamov on 12-Mar-17.
 */
public class ChunkParsingEngine {

    private ModifierService modifierService;
    private CalculationService calculationService;

    HashMap<String, CalculationData> chunkLocalStore = new HashMap<>();
    HashMap<String, PriorityQueue<Instrument>> chunkLocalPriorityQueue = new HashMap<>();

    public ChunkParsingEngine(CalculationService calculationService, ModifierService modifierService) {
        this.modifierService = modifierService;
        this.calculationService = calculationService;
    }
    /**
     * Will be reloaded Price Modifier if last updated period of value from database greater than in given seconds,
     * otherwise Price Modifier will be returned from cache.
     */
    public void byteChunkProcessing(Chunk chunk) {
        String line = null;
        try (BufferedReader reader = new BufferedReader(new InputStreamReader(new ByteArrayInputStream(chunk.getBytes())))) {
            line = reader.readLine();
            line = chunk.getPreviousBeginningPart() + line;
            while (line != null) {
                line = line.trim();
                Instrument instrument = Instrument.getInstance(line);
                if (instrument != null && instrument.isBusinessDay()) {
                    switch (instrument.getName()) {
                        case "INSTRUMENT1":
                            addValue(instrument, chunkLocalStore);
                            break;
                        case "INSTRUMENT2":
                            if (instrument.getDate().get(Calendar.MONTH) == Calendar.NOVEMBER) {
                                addValue(instrument, chunkLocalStore);
                            }
                            break;
                        case "INSTRUMENT3":
                            defineMaxValue(instrument, chunkLocalStore);
                            break;
                        default:
                            addIntoLocalPriorityQueue(instrument, chunkLocalPriorityQueue);
                            break;
                    }
                }
                line = reader.readLine();
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        calculationService.addLocalChunkCalculationDatasToGlobal(chunkLocalStore);
        calculationService.addLocalChunkPriorityQueueToGlobal(chunkLocalPriorityQueue);
    }


    private CalculationData getLastUpdatedPrice(Instrument instrument, HashMap<String, CalculationData> chunkLocalStore) {
        if (!chunkLocalStore.containsKey(instrument.getName())) {
            chunkLocalStore.put(instrument.getName(), new CalculationData());
        }
        CalculationData lastData = chunkLocalStore.get(instrument.getName());
        BigDecimal newPrice = modifierService.getPriceModifier(instrument.getName()).multiply(instrument.getValue());
        lastData.setValue(newPrice);
        return lastData;
    }

    /**
     * Calculates INSTRUMENT1 and INSTRUMENT2 objects sum and count.
     */
    private void addValue(Instrument instrument, HashMap<String, CalculationData> chunkLocalStore) {
        CalculationData lastData = getLastUpdatedPrice(instrument, chunkLocalStore);
        lastData.sumAndIncrement(lastData.getValue());
    }

    /**
     * Defines max value for INSTRUMENT3
     */
    private void defineMaxValue(Instrument instrument, HashMap<String, CalculationData> chunkLocalStore) {
        CalculationData lastData = getLastUpdatedPrice(instrument, chunkLocalStore);
        lastData.setMaxValue(lastData.getValue());
    }


    /**
     * As PriorityQueue functionality defines objects ordering by their Comparable implementation,
     * in this case Instrument object is compared with its date field. OTHER_INSTRUMENTS_NEWEST_ELEMENTS_COUNT is given for
     * number of elements which should be calculated related to other instruments excluding INSTRUMENT1, INSTRUMENT2, INSTRUMENT3.
     */
    private void addIntoLocalPriorityQueue(Instrument instrument, HashMap<String, PriorityQueue<Instrument>> chunkLocalOtherInstruments) {
        if (!chunkLocalOtherInstruments.containsKey(instrument.getName())) {
            chunkLocalOtherInstruments.put(instrument.getName(), new PriorityQueue<>(Parameters.OTHER_INSTRUMENTS_NEWEST_ELEMENTS_COUNT + 1));
        }
        chunkLocalOtherInstruments.get(instrument.getName()).add(instrument);
        if (chunkLocalOtherInstruments.get(instrument.getName()).size() > Parameters.OTHER_INSTRUMENTS_NEWEST_ELEMENTS_COUNT) {
            Instrument unused = chunkLocalOtherInstruments.get(instrument.getName()).poll();
            if (!instrument.equals(unused)) {
                instrument.setValue(modifierService.getPriceModifier(instrument.getName()).multiply(instrument.getValue()));
            }
        } else {
            instrument.setValue(modifierService.getPriceModifier(instrument.getName()).multiply(instrument.getValue()));
        }
    }
}
