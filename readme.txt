Brief description of architecture and work-flow as below:

Initially main thread does read given input file by chunks. Measure of unit is bytes. Length of bytes can be changed by parameter. 
I used NIO library to read file through Channel into ByteBuffer. Each time when ByteBuffer reads chunk bytes are kept as Chunk object.
Within Chunk object read bytes are separated two parts: first part is from beginning to last EOL as main part, second part is from EOL to end 
of buffer as remaining part. Next created Chunk object will use previous Chunk's remaining part to concatenate this as beginning of current 
Chunk object. This process continues while all file are read. Each time when Chunk object is ready is sent into parallel processing 
which are submitted via ExecutorService class. Thread counts also can be changed. In order to control multi threads count from exceeding 
maximum given count I used Semaphore. Before creating Chunk object I check thread pooling for being free with acquire method, then Chunk object 
goes into processing by submitting it through Executor Service. When mentioned thread finish its job release thread pooling. Duty of each thread 
is to parse Chunk object and to store all instruments as calculation data so are calculated its Count, Sum, Max value on depending concrete 
instrument. What is more when calculates final price of instrument I check modifier in order to multiply it. First of all I check is there 
appropriate instrument price modifier in the cache that update time not exceed given time. If not I get it from database and store in the cache. 
As all instruments from Chunk object calculated and stored by thread then Calculation Service is used to add these bulk into global store. 
Additionally to keep newest elements of other instruments apart from INSTRUMENT 1, 2, 3 I used Priority Queue due to ordering by date.


In order to run project you need to follow steps below:

1. Java 8 (required).
2. Install Apache Maven 3+ (I tested it in 3.2.3 version).
4. You can manage -Xmx heap size before running task via maven 
	For example: SET MAVEN_OPTS="-Xmx256m"
3. mvn test -Dexec.args="PATH_TO_FILE" (if you will not specified file path then system automatically will get example_input.text file from project folder).

First of all before running Main method all unit tests will be checked. In case of any fail program will stop.
During checking of database connection unit test system automatically will create ~/taskdb H2 database with initital datas from initial_data.sql.

I have tested program with 2.02 GB file with different 10 instruments

RESULTS were printed as below:

Name: INSTRUMENT1, Count: 1984229, Average: 76.052826454
Name: INSTRUMENT2, Count: 326145, Average: 76.081359341
Name: INSTRUMENT3, Max value: 150.542260512449300
Name: INSTRUMENT4, Count: 5, Sum: 724.4725186604600300
Name: INSTRUMENT5, Count: 5, Sum: 740.6791046775302200
Name: INSTRUMENT6, Count: 5, Sum: 746.6546926960443700
Name: INSTRUMENT7, Count: 5, Sum: 740.7642189658692600
Name: INSTRUMENT8, Count: 5, Sum: 743.4384543867412400
Name: INSTRUMENT9, Count: 5, Sum: 746.3139568448283700
Name: INSTRUMENT10, Count: 5, Sum: 726.8564627915739900

PERFORMANCE with one thread lasted 10:47 min, however 7 parallel threads lasted 02:56 min.

Please take into consideration below information also:
My computer system settings are:
Processor: Intel(R) Core(TM) i7-2670QM CPU @ 2.20GHz
Installed memory (RAM): 8.00 GB
System type: 64 bit Windows 8.1 OS

You can also find log file in logs folder under project folder.